**Gesammelte Informationen zur Zielsetzung, Rahmenbedingungen und Bewerbung zum begleitenden Konsultationsprozess zur Zielbilderarbeitung der OZG-Rahmenarchitektur durch das föderale IT-Architekturboard finden Sie hier:** [Informationsseite Zielbild OZG-Rahmenarchitektur Konsultation](https://bmi.usercontent.opencode.de/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/)

## Aktuelles
Bis zum 20. Oktober 2023 können Bewerbungen eingereicht werden. Ab dem 24. Oktober wird dieses Projekt des FIT-ABs als zentrale Austauschplattform zwischen Teilnehmenden zur Zielbilderarbeitung und den Impulsgebenden des Konsultationsprozesses genutzt.


## Mitmachen
> Wenn Sie Interesse haben, sich aktiv in den Konsultationsprozess zur Erarbeitung des Zielbildes der OZG-Rahmenarchitektur einzubringen, folgen Sie der untenstehenden Schritt-für-Schritt Anleitung, um Ihre Bewerbung einzureichen. **Die Bewerbungsfrist endet am 20. Oktober 2023.**

Die Ergebnisse des Konsultationsprozesses fließen in die Erarbeitung des Zielbilds ein. Beteiligte Interessenvertretende spielen als Impulsgeber und kritische Meinungsbildende in diesem Prozess eine entscheidende Rolle, sind aber keine politisch legitimierten Personen mit Entscheidungsbefugnis.

### Anleitung zur Einreichung Ihrer Bewerbung über die Plattform OpenCoDE
**Schritt 1:** Laden Sie das auszufüllende Dokument zur Bewerbung herunter und füllen Sie es aus: [Vorlage Bewerbung](Bewerbung_Konsultation-OZG-Rahmenarchitektur__Name_der_Organisation___Ihr_Name___1_.pdf)

**Schritt 2:** **Um Ihre Bewerbung einzureichen, legen Sie sich bitte einen Open CoDE Account an.** Nutzen Sie danach folgenden Link: [Ihre Bewerbung hochladen](https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/-/issues) oder klicken Sie im **linken Seitenregister** auf die Option "Issue" und wählen Sie anschließend **"Create Issue"** aus. 

![Anleitung Bild 1](Bewerbung_Schritt_1.png)

**Schritt 3:** Geben Sie alle wichtigen Dokumente und Informationen an:

- (1) Geben Sie einen **aussagekräftigen Titel** für Ihre Bewerbung ein (z. B. "Bewerbung zum Konsultationsprozess OZG-Rahmenarchitektur")
- (2) Laden Sie Ihre Bewerbung im **PDF-Format** hoch. 
   -- (A) Sie können die Datei entweder über den **"Hochladen"-Button** 
   -- (B) oder per **"Drag & Drop"** hochladen.
- (3) Tragen Sie den Namen des **"Assignee" (zuständige Person)** ein.
- (4) Wählen Sie das **Label "Bewerbung"** aus.
- (5) Bestätigen Sie Ihre Bewerbung, indem Sie auf **"Create Issue"** klicken.

![Anleitung Bild 2](Bewerbung_Schritt_2.png)


## Zeitplan des Konsultationsprozess

Die Auftaktveranstaltung zum Konsultationsprozess findet am 24. Oktober 2023 statt. Informationen zum weiteren Prozess, einschließlich der geplanten Termine für die themenspezifischen Online-Webseminare, sind dem beigefügten Zeitplan zu entnehmen.

> **Hinweis** Die genannten Termine sind zunächst als "Save the Date" zu verstehen. Weitere Veranstaltungsformate und Termine werden nach Bedarf ergänzt und entsprechend kommuniziert.

**September 2023**  

- [x] 20.09.      Start der Bewerbungsphase

**Oktober 2023**

- [ ] 20.10.23    Ende der Bewerbungsphase
- [ ] 24.10.23    Online-Webseminar I - Auftaktveranstaltung (15:30-17:00)
- [ ] 24.10.23    Start Online-Konsultation

**November 2023**   

Keine Termine zum Konsultationsprozess

**Dezember 2023**   

- [ ] 05.12.23    Online-Webseminar II - Mid-Review Veranstaltung (15:30-17:00)

**Januar 2024** 

- [ ] 18.01.24    Online-Webseminar III - Abschluss-Veranstaltung (15:30-17:00)
- [ ] 31.01.24    Ende Online-Konsultation

**Februar 2024**

- [ ] 15.02.24    Ergebnisbericht zu dem Konsultationsprozess


### ℹ️ 
**Bei weiteren Fragen zur Konsultation kontaktieren Sie uns gerne über unser Postfach: [Postfach OZG-Rahmenarchitektur](mailto:OZG-Rahmenarchitektur@bmi.bund.de)**