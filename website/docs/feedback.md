# Mitmachen
Wenn Sie Interesse haben, sich aktiv in den Konsultationsprozess des FIT-ABs zur Erarbeitung des Zielbildes der OZG-Rahmenarchitektur einzubringen, reichen Sie bitte Ihre Bewerbung über Open CoDE ein. **Die Bewerbungsfrist endet am 20. Oktober 2023.**


??? info "Welche Inhalte werden in der Bewerbung erwartet?"
    ## Welche Inhalte werden in der Bewerbung erwartet?
     - **1) Organisation:** Geben Sie eine kurze Vorstellung Ihrer Organisation, die am Konsultationsprozess teilnehmen möchte. 
     - **2) Name und E-Mail-Adresse** Ihres Vertreters oder Ihrer Vertreterin: Bitte nennen Sie uns den Namen und die E-Mail-Adresse der Person, die Ihre Gruppe oder Organisation in diesem Prozess vertreten wird.
     - **3) Motivationsangabe** (maximal 200 Wörter): Teilen Sie uns mit, warum Ihre Gruppe oder  Organisation an der Konsultation teilnehmen möchte und welchen Beitrag Sie dazu leisten können.
     Laden Sie sich hier das Bewerbungsformular herunter: [Bewerbungsformular](https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/-/blob/main/Bewerbung_Konsultation-OZG-Rahmenarchitektur__Name_der_Organisation___Ihr_Name___1_.pdf). Um Ihre Bewerbung einzureichen folgen Sie bitte der untenstehenden Anleitung.
                
??? info "Wo und wie reiche ich meinen Antrag ein?"
    ## Wo und wie reiche ich meinen Antrag ein?
     **Schritt 1:** Laden Sie das auszufüllende Dokument zur Bewerbung herunter und füllen Sie es aus: [Bewerbungsformular](https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/-/blob/main/Bewerbung_Konsultation-OZG-Rahmenarchitektur__Name_der_Organisation___Ihr_Name___1_.pdf).
        <figure markdown> 
        ![Anleitung Bild 2](https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/-/raw/main/Bewerbung_Schritt_1.png){ width="500"}
        </figure>
     **Schritt 2:** **Um Ihre Bewerbung einzureichen, legen Sie sich bitte einen [Open CoDE Account](https://opencode.de/de/registrieren) an** <br>
         Nutzen Sie folgenden Link: [Ihre Bewerbung hochladen](https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/-/issues) oder klicken Sie im **linken Seitenregister** auf die Option "Issue" und wählen Sie anschließend **"Create Issue"** aus. 
         <figure markdown>
         ![Anleitung Bild 2](https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/-/raw/main/Bewerbung_Schritt_2.png){ width="500"}
         </figure>
     **Schritt 3:** Geben Sie alle wichtigen Dokumente und Informationen an: <br>
         **(1) Geben Sie einen aussagekräftigen Titel für Ihre Bewerbung ein** (z. B. "Bewerbung zum Konsultationsprozess OZG-Rahmenarchitektur") <br>
         **(2) Laden Sie Ihre Bewerbung im PDF-Format hoch.**  <br>
          **(A)** Sie können die Datei entweder über den "Hochladen"-Button oder **(B)** per "Drag & Drop" hochladen. <br>
         **(3) Tragen Sie den Namen des "Assignee" (zuständige Person) ein.** <br>
         **(4) Wählen Sie das Label "Bewerbung" aus.** <br>
         **(5) Bestätigen Sie Ihre Bewerbung, indem Sie auf "Create Issue" klicken.** <br>


??? info "Formalia und Dateibenennung"
    ## Welche Informationen sollten in der Bewerbung enthalten sein? <br>
     Bitte achten Sie auf eine **einheitliche Benennung Ihres Dokumentes**, um eine bessere Übersichtlichkeit für alle zu gewährleisten: <br>
          "Bewerbung_Konsultation-OZG-Rahmenarchitektur_[Name der Organisation]_[Ihr Name]"


