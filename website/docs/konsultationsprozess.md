# Konsultationsprozess

## Ziele des Konsultationsprozesses
Mit dem Konsultationsprozess möchte das föderale IT-Architekturboard (FIT-AB) unterschiedliche Perspektiven berücksichtigen und sicherstellen, dass das erarbeitete Rahmenarchitektur-Zielbild die Bedürfnisse und Anforderungen verschiedener Nutzendengruppen erfüllt – ein entscheidender Erfolgsfaktor für eine effektive tragfähige und nutzendenorientierte OZG-Rahmenarchitektur. <br>
Des Weiteren verfolgt der Konsultationsprozess folgende Ziele: 

- **(1) Stakeholder-Beteiligung und Transparenz:** Relevante Beteiligte werden durch das FIT-AB einbezogen sowie Ergebnisse der Arbeitstermine klar und transparent kommuniziert und Möglichkeiten zur offenen Diskussion geschaffen.
- **(2) Interaktive Beteiligung und Weiterentwicklung:** Die aktive Einbindung der Mitwirkenden und deren kontinuierliches Feedback durch Online-Konsultationen während des Prozesses ermöglichen Rückführungen der Impulse aus dem Konsultationsprozess in die Zielbilderarbeitung durch das FIT-AB. 
- **(3) Identifizierung potenzieller White Spots:** Vollständige Informationsbasis durch umfassende Rückmeldungen.

## Durchführung
Im Verlauf der Zielbilderarbeitung des föderalen IT-Architekturboards (FIT-AB) werden in Arbeitsterminen Leitfragen entwickelt und in den Konsultationsprozess integriert, um wertvolles Feedback von den Konsultationsteilnehmenden zu erhalten.

Ab Oktober, finden monatlich interaktive Sitzungen statt, in denen Expertinnen und Experten die Arbeitsergebnisse des Zielbildes aus dem FIT-AB präsentieren und Raum für Fragen und Diskussionen bieten. Der eigentliche Konsultationsprozess erfolgt online durch die Veröffentlichung der Arbeitsergebnisse des FIT-ABs, welche von den Beteiligten aus dem Konsultationsprozess kommentiert werden können. Dadurch haben Teilnehmende über den gesamten Zeitraum hinweg die Möglichkeit, ihre Kommentare, Bedenken oder Empfehlungen über Open CoDE abzugeben. **Die Ergebnisse dieses Konsultationsprozesses werden von dem FIT-AB zurück in die Termine zur Zielerarbeitung geführt.** Beteiligte Interessenvertretende spielen als Impulsgebende und kritische Meinungsbildende in diesem Prozess eine entscheidende Rolle, sind aber keine politisch legitimierten Personen mit Entscheidungsbefugnis.

!!! info "Feedback und Impulse"
    Im gesamten Zeitraum vom 24. Oktober 2023 bis zum 31. Januar 2024 können die Konsultationsteilnehmenden die Ergebnisse sowie Entscheidungen zur Zielbilderarbeitung des FIT-ABs kommentieren. <br>

## Wichtige Daten

Ab dem **20.09.2023** haben Interessierte vier Wochen die Möglichkeit, sich mit einer Motivationsangabe für den Konsultationsprozess zu bewerben.

Ab dem **23.10.2023** werden berechtigte Teilnehmende eingeladen an den Online-Webseminaren und -konsultation teilzunehmen.

Ab dem **24.10.2023** startet die **Online-Konsultation**. Kommentare zu den Ergebnissen sowie Entscheidungen des Teilnehmendenkreises zum Vorhaben des FIT-ABs können von den Impulsgebenden laufend über Open CoDE kommentiert werden. In interaktiven **Online-Webseminare** werden im monatlichen Rhythmus  Expertinnen und Experten die Arbeitsergebnisse des Zielbildes präsentieren und Raum für Fragen und Diskussionen bieten. Voraussichtlich werden die Online-Webseminare (teilweise) nach Themenschwerpunkten gegliedert, ggf. werden noch weitere Termine angesetzt. <br>

Aktuell geplante Online-Webseminar-Termine sind: <br>
**1. Termin:** 24. Oktober 2023  <br>
**2. Termin:** 05. Dezember 2023  <br>
**3. Termin:** 18. Januar 2024  <br>


!!! info "Hinweis"
    Die Teilnehmerinnen und Teilnehmer des Konsultationsprozesses, gesteuert durch das FIT-AB, spielen eine entscheidende Rolle als Impulsgebende und kritische Meinungstragende, sind aber keine politisch legitimierten Personen mit Entscheidungsbefugnis. Der eigens zu diesem Zweck eingerichtete Teilnehmendenkreis zum Vorhaben der FIT-AB ist für die Entwicklung und Umsetzung der Methodik zur Integration der Ergebnisse dieser Konsultation verantwortlich. Dazu treffen sich dieser Teilnehmendenkreis alle drei Wochen zur inhaltlichen Erarbeitung des Zielbildes der OZG-Rahmenarchitektur. Die Ergebnisse und Beschlüsse dieser Arbeitstermine werden in zusammengefasster Form von dem FIT-AB auf Open CoDE veröffentlicht.

