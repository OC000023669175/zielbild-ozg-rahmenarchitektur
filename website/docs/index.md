<div class="bpa-module bpa-module--dark-eagle"><div class="bpa-container"><div class="bpa-row"><div class="bpa-col"><h1 class="bpa-heading bpa-heading--h1">Projekt Zielbild OZG-Rahmenarchitektur <br/> Konsultation des FIT-AB</h1></div></div></div></div>
<br/>
!!! info "Was Sie erwartet"
    Gesammelte Informationen zur Zielsetzung, Rahmenbedingungen und Bewerbung zum begleitenden Konsultationsprozess zur Zielbilderarbeitung der OZG-Rahmenarchitektur durch das föderale IT-Architekturboard finden Sie hier. 


## Unser Projekt

Im Rahmen der Erarbeitung des Zielbildes durch das föderale IT-Architekturboard (FIT-AB) wird von Oktober 2023 bis Januar 2024 ein begleitender und partizipativ gestalteter Konsultationsprozess stattfinden, bei dem Vertretende der Politik und Verwaltung, Wirtschaft, Wissenschaft sowie zivilgesellschaftliche Organisationen eingebunden werden.


## Ausgangslage und Zielsetzung

**Ziel des Onlinezugangsgesetzes (OZG) ist es, Verwaltungsleistungen für Bürgerinnen, Bürger, Unternehmen, Verwaltungsmitarbeitende und die Verwaltung selbst durch attraktive, nutzendenfreundliche digitale Angebote einfach, sicher und von überall und zu jedem Zeitpunkt nutzbar zu machen.**

Maßgeblich dafür ist ein Zielbild für eine ganzheitliche IT-Architektur, welche einen gemeinsamen Rahmen für die Verwaltungsdigitalisierung von Bund, Ländern und Kommunen definieren soll. Diese sogenannte "OZG-Rahmenarchitektur" umfasst alle notwendigen Standards, Schnittstellen sowie zentralen Basisdienste und -komponenten. Das föderale IT-Architekturboard (FIT-AB) mit Mitgliedern aus elf Ländern, des Bundes und der Föderalen IT-Kooperation (FITKO) übernimmt die inhaltliche Erarbeitung des Zielbildes für die gemeinsame OZG-Rahmenarchitektur, das zukünftig allen OZG-Umsetzenden als Orientierung dienen soll.

Dieser Zielbildprozess soll als Ausgangspunkt für einen Dialog mit weiteren relevanten Impulsgebenden dienen. Auf Basis des erarbeiteten Zielbildes soll in Zusammenarbeit von Bund und Ländern ein Umsetzungsprogramm („OZG-Infrastrukturprogramm 2.0“) erstellt werden, das einen wichtigen Beitrag zur Verwaltungsdigitalisierung in Deutschland leistet.


!!! info "Jetzt mitmachen und Bewerbung einreichen!" 
    Wenn Sie Interesse haben, sich aktiv in den Konsultationsprozess zur Erarbeitung des Zielbildes der OZG-Rahmenarchitektur einzubringen, können Sie sich bis zum 20. Oktober 2023 bewerben. Mehr Informationen zum Bewerbungsprozess: [Mitmachen](https://bmi.usercontent.opencode.de/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/feedback/). <br><br>
    
    > Die Open Source-Plattform der Öffentlichen Verwaltung Open CoDE soll dabei als Austauschplattform und zentrale Stelle für das Einholen von Feedback dienen: [OZG-Rahmenarchitektur auf Open CoDE](https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur).
