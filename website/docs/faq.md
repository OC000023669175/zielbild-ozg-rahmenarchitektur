# Frequently Asked Questions

??? info "Was ist der Konsultationsprozess?"
    ## Was ist der Konsultationsprozess?
    Der Konsultationsprozess wird genutzt, um eine breite Beteiligung und Transparenz in Entscheidungsprozessen zu fördern sowie sicherzustellen, dass die Interessen vieler verschiedener Nutzendengruppen Berücksichtigung finden und in ein nutzendenzentriertes Zielbild einfließen können.
    Der Konsultationsprozess des FIT-ABs zielt darauf ab, verschiedene Interessensgruppen, darunter Vertreterinnen und Vertreter von zivilgesellschaftlichen Organisationen, Privatwirtschaft, Wissenschaft sowie der Politik und Verwaltung, aktiv in die Erarbeitung des Zielbildes der OZG-Rahmenarchitektur einzubeziehen. 

??? info "Worüber wird im Konsultationsprozess gesprochen?"
    ## Worüber wird im Konsultationsprozess gesprochen? 
     In einem kontinuierlichen Austausch werden die veröffentlichten Ergebnisse aus den Arbeitsterminen des FIT-ABs sowie der aktuelle Stand der Erarbeitung des OZG-Rahmenarchitektur-Zielbildes besprochen. 

??? info "Was ist die OZG-Rahmenarchitektur?" 
    ##Was ist die OZG-Rahmenarchitektur?        
     **Ziel des Onlinezugangsgesetzes (OZG) ist es, Verwaltungsleistungen für Bürgerinnen, Bürger, Unternehmen, Verwaltungsmitarbeitende und die Verwaltung selbst durch attraktive, nutzendenfreundliche digitale Angebote einfach, sicher und von überall und zu jedem Zeitpunkt nutzbar zu machen.**
     Maßgeblich dafür ist eine ganzheitliche IT-Architektur, welche einen Rahmen für die Verwaltungsdigitalisierung von Bund, Länder und Kommunen definieren soll. 
     Diese sogenannte "OZG-Rahmenarchitektur" umfasst alle notwendigen Standards, Schnittstellen sowie zentralen Basisdienste und -komponenten, die eine zielgerichtete Umsetzung des OZG sicherstellen soll. Das dazu erarbeitete Zielbild für die gemeinsame OZG-Rahmenarchitektur soll zukünftig allen OZG-Umsetzenden als Orientierung dienen.
    

??? info "Wie lade ich meine Bewerbung hoch?"
    ## Wie lade ich meine Bewerbung hoch? <br>
     Um eine Bewerbung einzureichen folgen Sie bitte der [Schritt-für-Schritt-Anleitung](https://bmi.usercontent.opencode.de/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/feedback/). Bitte beachten Sie, dass Sie sich vorab einen Open CoDE Account anlegen müssen. <br> 
      

??? info "Welche Inhalte werden in der Bewerbung erwartet?"
    ## Welche Inhalte werden in der Bewerbung erwartet?
     Alle notwendigen Informationen für Ihre Bewerbung finden Sie im [Bewerbungsformular](https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/-/blob/main/Bewerbung_Konsultation-OZG-Rahmenarchitektur__Name_der_Organisation___Ihr_Name___1_.pdf). <br>
     Für mehr Informationen [klicken Sie hier](https://bmi.usercontent.opencode.de/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/konsultationsprozess/)


??? info "In welchem Rahmen finden die **zukünftigen Online-Webseminare** statt?"
    ## In welchem Rahmen finden die zukünftigen Online-Webseminare statt?
     Im Konsultationsprozess sollen mehrere Online-Webseminare mit den Teilnehmenden durchgeführt werden. Voraussichtlich werden die Online-Webseminare (teilweise) nach Themenschwerpunkten gegliedert, ggf. werden noch weitere Termine angesetzt. <br>
     Aktuell geplante Online-Webseminar-Termine sind: <br>
     **1. Termin:** 24. Oktober 2023  <br>
     **2. Termin:** 05. Dezember 2023  <br>
     **3. Termin:** 18. Januar 2024  <br>

??? info "Wie lange läuft der Konsultationsprozess?"
    ## Wie lange läuft der Konsultationsprozess? 
     Der Konsultationsprozess ist angesetzt von Oktober 2023 bis Januar 2024. 

!!! info "Kontaktieren Sie uns"
    Bei weiteren Fragen zur Konsultation kontaktieren Sie uns gerne über unser Postfach: [Postfach OZG-Rahmenarchitektur](mailto:OZG-Rahmenarchitektur@bmi.bund.de)